//
//  TickerService.swift
//  BarakaTestTask
//
//  Created by Milkhail Babushkin on 28.04.2022.
//

import Foundation
import Combine

protocol TickerServiceProtocol {
    func currentTickers(forceUpdate: Bool) -> AnyPublisher<[Ticker], Error>
}

// This service simulate stocks changes by picking random values
final class TickerService: TickerServiceProtocol {
    private var cache: [String: [Ticker]]?
    private let mapper: TickerMapperProtocol
    private let networkManager: NetworkManagerProtocol

    init(
        networkManager: NetworkManagerProtocol,
        mapper: TickerMapperProtocol
    ) {
        self.networkManager = networkManager
        self.mapper = mapper
    }

    func currentTickers(forceUpdate: Bool) -> AnyPublisher<[Ticker], Error> {
        if cache != nil, !forceUpdate {
            return Just(pickRandomTickers())
                .setFailureType(to: Error.self)
                .eraseToAnyPublisher()
        } else {
            return networkManager.requestData(endpoitable: Endpoints.ticker.endpoint)
                .map { String(data: $0, encoding: .utf8) ?? "" }
                .map(mapper.map(tickerResponse:))
                .map(saveArrayToCache(array:))
                .map(pickRandomTickers)
                .eraseToAnyPublisher()
        }
    }

    private func saveArrayToCache(array: [Ticker]){
        cache = Dictionary(grouping: array, by: {
            $0.title
        })
    }

    private func pickRandomTickers() -> [Ticker] {
        var result: [Ticker] = []
        guard let cache = cache else {
            return []
        }

        for key in cache.keys {
            let index = Int.random(in: 0..<cache[key]!.count)
            result.append(cache[key]![index])
        }

        return result
    }
}
