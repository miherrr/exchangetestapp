//
//  NewsService.swift
//  BarakaTestTask
//
//  Created by Milkhail Babushkin on 28.04.2022.
//

import Foundation
import Combine

protocol NewsServiceProtocol {
    func feed() -> AnyPublisher<[Article], Error>
}

final class NewsService: NewsServiceProtocol {
    private let feedMapper: FeedMapperProtocol
    private let networkManager: NetworkManagerProtocol

    init(
        feedMapper: FeedMapperProtocol,
        networkManager: NetworkManagerProtocol
    ) {
        self.feedMapper = feedMapper
        self.networkManager = networkManager
    }

    func feed() -> AnyPublisher<[Article], Error> {
        networkManager.requestModel(endpoitable: Endpoints.feed.endpoint)
            .map(feedMapper.map(feedResponse:))
            .eraseToAnyPublisher()
    }
}
