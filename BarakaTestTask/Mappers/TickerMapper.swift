//
//  TickerMapper.swift
//  BarakaTestTask
//
//  Created by Milkhail Babushkin on 28.04.2022.
//

import Foundation
import SwiftCSV

protocol TickerMapperProtocol {
    func map(tickerResponse: String) -> [Ticker]
    func map(domainModel: Ticker) -> TickerCollectionViewCellModel
}

class TickerMapper: TickerMapperProtocol {
    private let dateFormatter: DateFormatter

    init() {
        dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .short
        dateFormatter.timeStyle = .none
    }

    func map(tickerResponse: String) -> [Ticker] {
        let csv = try? CSV(string: tickerResponse)

        return csv?.enumeratedRows.compactMap { tickerItemResponse in
            guard
                tickerItemResponse.count >= 2,
                let diff = Double(tickerItemResponse[1])
            else {
                return nil
            }
            return Ticker(title: tickerItemResponse[0], diff: diff)
        } ?? []
    }

    func map(domainModel: Ticker) -> TickerCollectionViewCellModel {
        TickerCollectionViewCellModel(title: domainModel.title, rate: String(format: "%.1f", domainModel.diff) + " USD")
    }
}
