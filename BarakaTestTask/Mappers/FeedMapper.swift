//
//  FeedMapper.swift
//  BarakaTestTask
//
//  Created by Milkhail Babushkin on 28.04.2022.
//

import Foundation

protocol FeedMapperProtocol {
    func map(feedResponse: FeedResponse) -> [Article]
    func map(domainModel: Article) -> TopNewsCollectionViewModel
    func map(domainModel: Article) -> NewsCollectionViewModel
}

final class FeedMapper: FeedMapperProtocol {
    private let dateFormatter: DateFormatter
    private let shortDateFormatter: DateFormatter

    init() {
        dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"

        shortDateFormatter = DateFormatter()
        shortDateFormatter.timeStyle = .short
        shortDateFormatter.dateStyle = .short
    }

    func map(feedResponse: FeedResponse) -> [Article] {
        feedResponse.articles.compactMap { articleResponse in
            guard
                let url = URL(string: articleResponse.url),
                let date = dateFormatter.date(from: articleResponse.publishedAt)
            else {
                return nil
            }

            return Article(
                source: Article.Source(
                    id: articleResponse.source.id,
                    name: articleResponse.source.name
                ),
                author: articleResponse.author,
                title: articleResponse.title,
                description: articleResponse.description,
                url: url,
                imageUrl: URL(string: articleResponse.urlToImage ?? ""),
                publishedAt: date,
                content: articleResponse.content
            )
        }
    }

    func map(domainModel: Article) -> TopNewsCollectionViewModel {
        TopNewsCollectionViewModel(news: domainModel.title, imageUrl: domainModel.imageUrl)
    }

    func map(domainModel: Article) -> NewsCollectionViewModel {
        NewsCollectionViewModel(
            title: domainModel.title,
            date: shortDateFormatter.string(from: domainModel.publishedAt),
            imageUrl: domainModel.imageUrl,
            content: domainModel.content
        )
    }
}
