//
//  NetworkManager.swift
//  BarakaTestTask
//
//  Created by Milkhail Babushkin on 28.04.2022.
//

import Combine
import Foundation

enum HTTPMethod: String {
    case GET
    case POST
}

protocol Endpoitable {
    var url: URL { get }
    var httpMethod: HTTPMethod { get }
}

struct Endpoint: Endpoitable {
    let url: URL
    let httpMethod: HTTPMethod
}

enum Endpoints {
    case ticker, feed

    var endpoint: Endpoitable {
        switch self {
        case .ticker:
            return Endpoint(url: URL(string: "https://raw.githubusercontent.com/dsancov/TestData/main/stocks.csv")!, httpMethod: .GET)
        case .feed:
            return Endpoint(url: URL(string: "https://saurav.tech/NewsAPI/everything/cnn.json")!, httpMethod: .GET)
        }
    }
}

protocol NetworkManagerProtocol {
    func requestModel<Response: Decodable>(endpoitable: Endpoitable) -> AnyPublisher<Response, Error>
    func requestData(endpoitable: Endpoitable) -> AnyPublisher<Data, Error>
}

class NetworkManager: NetworkManagerProtocol {
    private let urlSession: URLSession
    private let decoder: JSONDecoder

    init(
        urlSession: URLSession,
        decoder: JSONDecoder
    ) {
        self.urlSession = urlSession
        self.decoder = decoder
    }

    func requestModel<Response>(endpoitable: Endpoitable) -> AnyPublisher<Response, Error> where Response : Decodable {
        return requestData(endpoitable: endpoitable)
            .decode(type: Response.self, decoder: decoder)
            .mapError({ error in
                print(error)
                return error
            })
            .eraseToAnyPublisher()
    }

    func requestData(endpoitable: Endpoitable) -> AnyPublisher<Data, Error> {
        var urlRequest = URLRequest(url: endpoitable.url)
        urlRequest.httpMethod = endpoitable.httpMethod.rawValue

        return urlSession.dataTaskPublisher(for: urlRequest)
            .map { data, _ in data }
            .mapError { $0 as Error }
            .eraseToAnyPublisher()
    }
}
