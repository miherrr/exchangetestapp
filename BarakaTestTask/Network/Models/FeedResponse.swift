//
//  TickerResponse.swift
//  BarakaTestTask
//
//  Created by Milkhail Babushkin on 28.04.2022.
//

import Foundation

struct FeedResponse: Decodable {
    let status: String
    let totalResults: Int
    let articles: [Article]
}

extension FeedResponse {
    struct Article: Decodable {
        let source: Source
        let author: String?
        let title: String?
        let description: String
        let url: String
        let urlToImage: String?
        let publishedAt: String
        let content: String
    }
}

extension FeedResponse.Article {
    struct Source: Decodable {
        let id: String
        let name: String
    }
}
