//
//  ImageDownloader.swift
//  BarakaTestTask
//
//  Created by Milkhail Babushkin on 29.04.2022.
//

import Foundation
import UIKit
import Combine

protocol ImageDownloaderProtocol {
    func getCached(forUrl url: URL) -> UIImage?
    func loadImage(url: URL) -> AnyPublisher<UIImage?, Error>
}

class DefaultImageDownloader: ImageDownloaderProtocol {
    private var cache = [String: UIImage]()

    static let shared = DefaultImageDownloader()

    func getCached(forUrl url: URL) -> UIImage? {
        cache[url.absoluteString]
    }

    func loadImage(url: URL) -> AnyPublisher<UIImage?, Error> {
        if let cachedImage = cache[url.absoluteString] {
            return Just(cachedImage).setFailureType(to: Error.self).eraseToAnyPublisher()
        } else {
            return URLSession.shared.dataTaskPublisher(for: url)
                .map { [weak self] data, _ in
                    let image = UIImage(data: data)
                    self?.cache[url.absoluteString] = image
                    return image
                }
                .mapError { $0 as Error }
                .eraseToAnyPublisher()
        }
    }
}
