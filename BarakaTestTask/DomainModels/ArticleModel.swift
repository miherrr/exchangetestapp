//
//  ArticleModel.swift
//  BarakaTestTask
//
//  Created by Milkhail Babushkin on 28.04.2022.
//

import Foundation

struct Article {
    let source: Source
    let author: String?
    let title: String?
    let description: String
    let url: URL
    let imageUrl: URL?
    let publishedAt: Date
    let content: String
}

extension Article {
    struct Source {
        let id: String
        let name: String
    }
}
