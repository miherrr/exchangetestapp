//
//  TickerModel.swift
//  BarakaTestTask
//
//  Created by Milkhail Babushkin on 28.04.2022.
//

struct Ticker {
    let title: String
    let diff: Double
}
