//
//  TopNewsCollectionViewCell.swift
//  BarakaTestTask
//
//  Created by Milkhail Babushkin on 28.04.2022.
//

import UIKit

struct TopNewsCollectionViewModel: Hashable {    
    let news: String?
    let imageUrl: URL?
}

final class TopNewsCollectionViewCell: UICollectionViewCell {
    static let reuseIdentifier = "TopNewsCollectionViewCell"

    private var newsTitle: UILabel!
    private var imageView: ImageViewLoadable!

    override init(frame: CGRect) {
        super.init(frame: frame)

        setupSubviews()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func prepareForReuse() {
        super.prepareForReuse()

        newsTitle.text = nil
        imageView.image = nil
    }

    func setViewModel(_ viewModel: TopNewsCollectionViewModel) {
        self.newsTitle.text = viewModel.news
        if let imageUrl = viewModel.imageUrl {
            self.imageView.setImage(url: imageUrl)
        }
    }

    private func setupSubviews() {
        contentView.layer.borderWidth = 1
        contentView.layer.borderColor = UIColor.black.cgColor

        newsTitle = UILabel()
        imageView = ImageViewLoadable()
        imageView.contentMode = .scaleAspectFit

        newsTitle.translatesAutoresizingMaskIntoConstraints = false
        imageView.translatesAutoresizingMaskIntoConstraints = false

        contentView.addSubview(newsTitle)
        contentView.addSubview(imageView)

        NSLayoutConstraint.activate([
            newsTitle.centerXAnchor.constraint(equalTo: contentView.centerXAnchor),
            newsTitle.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 8),
            newsTitle.leadingAnchor.constraint(greaterThanOrEqualTo: contentView.leadingAnchor),
            newsTitle.trailingAnchor.constraint(greaterThanOrEqualTo: contentView.trailingAnchor),
            imageView.topAnchor.constraint(equalTo: newsTitle.bottomAnchor, constant: 8),
            imageView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            imageView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            imageView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor)
        ])
    }
}
