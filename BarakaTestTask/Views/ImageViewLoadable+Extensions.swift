//
//  UIImageView+Extensions.swift
//  BarakaTestTask
//
//  Created by Milkhail Babushkin on 29.04.2022.
//

import Foundation
import UIKit

extension ImageViewLoadable {
    func setImage(url: URL, downloader: ImageDownloaderProtocol = DefaultImageDownloader.shared) {
        if let cached = downloader.getCached(forUrl: url) {
            image = cached
        } else {
            addLoader()
            loadImageSubscription = downloader.loadImage(url: url)
                .receive(on: RunLoop.main)
                .sink(receiveCompletion: { _ in }, receiveValue: { [weak self] image in
                    self?.hideLoader()
                    self?.image = image
                })
        }
    }
}
