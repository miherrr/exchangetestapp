//
//  ImageViewLoadable.swift
//  BarakaTestTask
//
//  Created by Milkhail Babushkin on 29.04.2022.
//

import Combine
import UIKit

class ImageViewLoadable: UIImageView {
    var loadImageSubscription: AnyCancellable?
    private weak var activityIndicator: UIActivityIndicatorView?

    func addLoader() {
        let activityIndicator = UIActivityIndicatorView(style: .medium)
        self.activityIndicator = activityIndicator
        addSubview(activityIndicator)
        activityIndicator.startAnimating()

        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            activityIndicator.centerXAnchor.constraint(equalTo: centerXAnchor),
            activityIndicator.centerYAnchor.constraint(equalTo: centerYAnchor)
        ])

    }

    func hideLoader() {
        activityIndicator?.removeFromSuperview()
    }
}
