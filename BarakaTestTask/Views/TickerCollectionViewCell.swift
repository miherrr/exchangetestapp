//
//  TickerCollectionViewCell.swift
//  BarakaTestTask
//
//  Created by Milkhail Babushkin on 28.04.2022.
//

import UIKit

struct TickerCollectionViewCellModel: Hashable {
    let title: String
    let rate: String
}

final class TickerCollectionViewCell: UICollectionViewCell {
    static let reuseIdentifier = "TickerCollectionViewCell"

    private var titleLabel: UILabel!
    private var rateLabel: UILabel!

    override init(frame: CGRect) {
        super.init(frame: frame)

        setupSubviews()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func prepareForReuse() {
        super.prepareForReuse()

        titleLabel.text = nil
        rateLabel.text = nil
    }

    func setViewModel(_ viewModel: TickerCollectionViewCellModel) {
        self.titleLabel.text = viewModel.title
        self.rateLabel.text = viewModel.rate
    }

    private func setupSubviews() {
        contentView.layer.borderColor = UIColor.black.cgColor
        contentView.layer.borderWidth = 1

        titleLabel = UILabel()
        rateLabel = UILabel()

        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        rateLabel.translatesAutoresizingMaskIntoConstraints = false

        contentView.addSubview(titleLabel)
        contentView.addSubview(rateLabel)

        NSLayoutConstraint.activate([
            titleLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 8),
            titleLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 8),
            rateLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 8),
            rateLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 8)
        ])
    }
}
