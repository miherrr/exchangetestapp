//
//  NewsCollectionViewCell.swift
//  BarakaTestTask
//
//  Created by Milkhail Babushkin on 28.04.2022.
//

import UIKit

struct NewsCollectionViewModel: Hashable {
    let title: String?
    let date: String
    let imageUrl: URL?
    let content: String
}

final class NewsCollectionViewCell: UICollectionViewCell {
    static let reuseIdentifier = "NewsCollectionViewCell"

    private var titleLabel: UILabel!
    private var dateLabel: UILabel!
    private var imageView: ImageViewLoadable!
    private var contentLabel: UILabel!

    override init(frame: CGRect) {
        super.init(frame: frame)

        setupSubviews()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func prepareForReuse() {
        super.prepareForReuse()

        titleLabel.text = nil
        dateLabel.text = nil
        contentLabel.text = nil
        imageView.image = nil
    }

    func setViewModel(_ viewModel: NewsCollectionViewModel) {
        self.titleLabel.text = viewModel.title
        self.dateLabel.text = viewModel.date
        self.contentLabel.text = viewModel.content

        if let imageUrl = viewModel.imageUrl {
            self.imageView.setImage(url: imageUrl)
        }
    }

    private func setupSubviews() {
        contentView.layer.borderWidth = 1
        contentView.layer.borderColor = UIColor.black.cgColor

        titleLabel = UILabel()
        dateLabel = UILabel()
        imageView = ImageViewLoadable()
        imageView.contentMode = .scaleAspectFit
        contentLabel = UILabel()

        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        dateLabel.translatesAutoresizingMaskIntoConstraints = false
        imageView.translatesAutoresizingMaskIntoConstraints = false
        contentLabel.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.setContentCompressionResistancePriority(.defaultLow, for: .horizontal)

        addSubview(titleLabel)
        addSubview(dateLabel)
        addSubview(imageView)
        addSubview(contentLabel)

        NSLayoutConstraint.activate([
            imageView.trailingAnchor.constraint(equalTo: trailingAnchor),
            imageView.topAnchor.constraint(equalTo: contentView.topAnchor),
            imageView.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 1/5),
//            imageView.widthAnchor.constraint(equalToConstant: 200),
//            imageView.heightAnchor.constraint(equalToConstant: 200),
            imageView.heightAnchor.constraint(equalTo: imageView.widthAnchor),
            titleLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            titleLabel.trailingAnchor.constraint(lessThanOrEqualTo: imageView.leadingAnchor),
            titleLabel.topAnchor.constraint(equalTo: contentView.topAnchor),
            dateLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            dateLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 4),
            dateLabel.trailingAnchor.constraint(lessThanOrEqualTo: imageView.leadingAnchor),
            contentLabel.topAnchor.constraint(greaterThanOrEqualTo: imageView.bottomAnchor, constant: 4),
            contentLabel.topAnchor.constraint(greaterThanOrEqualTo: dateLabel.bottomAnchor, constant: 4),
            contentLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            contentLabel.trailingAnchor.constraint(lessThanOrEqualTo: contentView.trailingAnchor)
        ])
    }
}
