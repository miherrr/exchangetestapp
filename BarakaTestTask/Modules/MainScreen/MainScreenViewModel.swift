//
//  ViewModel.swift
//  BarakaTestTask
//
//  Created by Milkhail Babushkin on 28.04.2022.
//

import Foundation
import Combine

protocol MainScreenViewModelProtocol {
    var tickerViewModels: [TickerCollectionViewCellModel] { get }
    var topNewsViewModels: [TopNewsCollectionViewModel] { get }
    var newsViewModels: [NewsCollectionViewModel] { get }

    var tickersUpdatePublisher: AnyPublisher<Void, Error> { get }
    var topNewsUpdatePublisher: AnyPublisher<Void, Error> { get }
    var newsUpdatePublisher: AnyPublisher<Void, Error> { get }

    func loadData()
}

class MainScreenViewModel: MainScreenViewModelProtocol {
    private(set) var tickerViewModels: [TickerCollectionViewCellModel] = []
    private(set) var topNewsViewModels: [TopNewsCollectionViewModel] = []
    private(set) var newsViewModels: [NewsCollectionViewModel] = []

    private(set) var tickersUpdatePublisher: AnyPublisher<Void, Error>
    private(set) var topNewsUpdatePublisher: AnyPublisher<Void, Error>
    private(set) var newsUpdatePublisher: AnyPublisher<Void, Error>

    private(set) var tickersUpdatePublisherInternal = PassthroughSubject<Void, Never>()
    private(set) var topNewsUpdatePublisherInternal = PassthroughSubject<Void, Never>()
    private(set) var newsUpdatePublisherInternal = PassthroughSubject<Void, Never>()

    private let tickerService: TickerServiceProtocol
    private let newsService: NewsServiceProtocol
    private let feedMapper: FeedMapperProtocol
    private let tickerMapper: TickerMapperProtocol

    private var subscriptions = Set<AnyCancellable>()

    init(
        tickerService: TickerServiceProtocol,
        newsService: NewsServiceProtocol,
        feedMapper: FeedMapperProtocol,
        tickerMapper: TickerMapperProtocol
    ) {
        self.tickerService = tickerService
        self.newsService = newsService
        self.feedMapper = feedMapper
        self.tickerMapper = tickerMapper

        tickersUpdatePublisher = tickersUpdatePublisherInternal.setFailureType(to: Error.self).eraseToAnyPublisher()
        topNewsUpdatePublisher = topNewsUpdatePublisherInternal.setFailureType(to: Error.self).eraseToAnyPublisher()
        newsUpdatePublisher = newsUpdatePublisherInternal.setFailureType(to: Error.self).eraseToAnyPublisher()
    }

    func loadData() {
        tickerService.currentTickers(forceUpdate: false)
            .receive(on: RunLoop.main)
            .sink(
                receiveCompletion: { _ in },
                receiveValue: { [weak self] tickers in
                    guard let self = self else {
                        return
                    }
                    self.tickerViewModels = tickers.map(self.tickerMapper.map(domainModel:))
                    self.tickersUpdatePublisherInternal.send()
                    self.startUpdate()
                })
            .store(in: &subscriptions)

        newsService.feed()
            .receive(on: RunLoop.main)
            .sink(
                receiveCompletion: { _ in },
                receiveValue: { [weak self] articles in
                    guard let self = self else {
                        return
                    }
                    self.topNewsViewModels = articles.prefix(6).map(self.feedMapper.map(domainModel:))
                    self.newsViewModels = articles.dropFirst(6).map(self.feedMapper.map(domainModel:))
                    self.topNewsUpdatePublisherInternal.send()
                    self.newsUpdatePublisherInternal.send()
                }
            )
            .store(in: &subscriptions)
    }

    private func startUpdate() {
        Timer.publish(every: 1, on: .main, in: .common)
            .autoconnect()
            .flatMap { [weak self] _ -> AnyPublisher<[Ticker], Error> in
                guard let self = self else {
                    return Fail(error: PublisherError.masterReleased).eraseToAnyPublisher()
                }
                return self.tickerService.currentTickers(forceUpdate: false) }
            .receive(on: RunLoop.main)
            .sink(
                receiveCompletion: { _ in },
                receiveValue: { [weak self] tickers in
                    guard let self = self else {
                        return
                    }
                    self.tickerViewModels = tickers.map(self.tickerMapper.map(domainModel:))
                    self.tickersUpdatePublisherInternal.send()
                })
            .store(in: &subscriptions)
    }
}


public enum PublisherError: Error {
    /// Error means using if self is died at clouser
    case masterReleased
}
