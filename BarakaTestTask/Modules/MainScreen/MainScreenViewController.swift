//
//  ViewController.swift
//  BarakaTestTask
//
//  Created by Milkhail Babushkin on 28.04.2022.
//

import Foundation
import Combine
import UIKit

final class MainScreenViewController: UIViewController {
    typealias Snapshot = NSDiffableDataSourceSnapshot<MainScreenSectionsProvider.Section, CollectionViewItem>

    private var collectionView: UICollectionView!

    private var viewModel: MainScreenViewModelProtocol
    private lazy var dataSource: UICollectionViewDiffableDataSource = configureDataSource()

    private var subscriptions = Set<AnyCancellable>()

    init(viewModel: MainScreenViewModelProtocol) {
        self.viewModel = viewModel

        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        setupSubviews()
        registerCells()

        viewModel.loadData()

        subscribeToDataChanges()
    }

    func applySnapshot(animatingDifferences: Bool = true) {
        // 2
        var snapshot = Snapshot()
        snapshot.appendSections([.rates, .topNews, .news])
        snapshot.appendItems(viewModel.tickerViewModels.map { .ticker($0) }, toSection: .rates)
        snapshot.appendItems(viewModel.topNewsViewModels.map { .topNews($0) }, toSection: .topNews)
        snapshot.appendItems(viewModel.newsViewModels.map { .news($0) }, toSection: .news)
        dataSource.apply(snapshot, animatingDifferences: animatingDifferences)
    }

    private func setupSubviews() {
        collectionView = UICollectionView(frame: .zero, collectionViewLayout: MainScreenSectionsProvider.createLayout())
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.dataSource = dataSource
        collectionView.delegate = self
        view.addSubview(collectionView)

        NSLayoutConstraint.activate([
            collectionView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
            collectionView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            collectionView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor),
            collectionView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])

        view.backgroundColor = .white
    }

    func configureDataSource() -> UICollectionViewDiffableDataSource<MainScreenSectionsProvider.Section, CollectionViewItem> {
        let dataSource = UICollectionViewDiffableDataSource<MainScreenSectionsProvider.Section, CollectionViewItem>(collectionView: collectionView) { (collectionView, indexPath, icon) -> UICollectionViewCell? in

            guard let section = MainScreenSectionsProvider.Section(rawValue: indexPath.section) else {
                return UICollectionViewCell()
            }

            switch section {
            case .rates:
                return collectionView.dequeueReusableCell(withReuseIdentifier: TickerCollectionViewCell.reuseIdentifier, for: indexPath)
            case .topNews:
                return collectionView.dequeueReusableCell(withReuseIdentifier: TopNewsCollectionViewCell.reuseIdentifier, for: indexPath)
            case .news:
                return collectionView.dequeueReusableCell(withReuseIdentifier: NewsCollectionViewCell.reuseIdentifier, for: indexPath)
            }
        }

        return dataSource
    }

    private func registerCells() {
        collectionView.register(TickerCollectionViewCell.self, forCellWithReuseIdentifier: TickerCollectionViewCell.reuseIdentifier)
        collectionView.register(TopNewsCollectionViewCell.self, forCellWithReuseIdentifier: TopNewsCollectionViewCell.reuseIdentifier)
        collectionView.register(NewsCollectionViewCell.self, forCellWithReuseIdentifier: NewsCollectionViewCell.reuseIdentifier)
    }

    private func subscribeToDataChanges() {
        viewModel.tickersUpdatePublisher
            .sink(receiveCompletion: { _ in }, receiveValue: { [weak self] _ in
                self?.applySnapshot()
            })
            .store(in: &subscriptions)

        viewModel.topNewsUpdatePublisher
            .sink(receiveCompletion: { _ in }, receiveValue: { [weak self] _ in
                self?.applySnapshot()
            })
            .store(in: &subscriptions)

        viewModel.newsUpdatePublisher
            .sink(receiveCompletion: { _ in }, receiveValue: { [weak self] _ in
                self?.applySnapshot()
            })
            .store(in: &subscriptions)
    }
}

extension MainScreenViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        guard let section = MainScreenSectionsProvider.Section(rawValue: indexPath.section) else {
            return
        }

        switch section {
        case .rates:
            guard let cell = cell as? TickerCollectionViewCell else {
                return
            }

            cell.setViewModel(viewModel.tickerViewModels[indexPath.row])
        case .topNews:
            guard let cell = cell as? TopNewsCollectionViewCell else {
                return
            }
            
            cell.setViewModel(viewModel.topNewsViewModels[indexPath.row])
        case .news:
            guard let cell = cell as? NewsCollectionViewCell else {
                return
            }

            cell.setViewModel(viewModel.newsViewModels[indexPath.row])
        }
    }
}

enum CollectionViewItem: Hashable {
    case ticker(TickerCollectionViewCellModel)
    case topNews(TopNewsCollectionViewModel)
    case news(NewsCollectionViewModel)
}
