//
//  SceneDelegate.swift
//  BarakaTestTask
//
//  Created by Milkhail Babushkin on 28.04.2022.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?


    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        if let windowScene = scene as? UIWindowScene {
            let window = UIWindow(windowScene: windowScene)

            let networkManager = NetworkManager(urlSession: .shared, decoder: JSONDecoder())
            let feedMapper = FeedMapper()
            let tickersMapper = TickerMapper()
            let viewModel = MainScreenViewModel(
                tickerService: TickerService(
                    networkManager: networkManager,
                    mapper: tickersMapper
                ),
                newsService: NewsService(
                    feedMapper: feedMapper,
                    networkManager: networkManager
                ),
                feedMapper: feedMapper,
                tickerMapper: tickersMapper
            )
            let viewController = MainScreenViewController(viewModel: viewModel)
            window.rootViewController = viewController

            self.window = window
            window.makeKeyAndVisible()
        }
    }

    func sceneDidEnterBackground(_ scene: UIScene) {
        // Called as the scene transitions from the foreground to the background.
        // Use this method to save data, release shared resources, and store enough scene-specific state information
        // to restore the scene back to its current state.

        // Save changes in the application's managed object context when the application transitions to the background.
        (UIApplication.shared.delegate as? AppDelegate)?.saveContext()
    }


}

